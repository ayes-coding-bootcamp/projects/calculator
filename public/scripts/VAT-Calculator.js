/*

The code implements a simple VAT calculator
that can be used to calculate the net amount (price exclusive of VAT),
gross amount (price inclusive of VAT),
and the VAT amount of a product based on different tax rates.

*/

// A function that takes a value and returns it as a string in Euro format
const convertToEuro = (value) => value.toFixed(2).concat("€");

// A function that takes a string and returns a number with only digits and decimal points
const parseCurrencyString = (str) => Number(str.replace(/[^0-9.-]+/g, "")) || 0;

// Declare a variable to hold the mode value
let modeValue;

// An array of objects, each representing a VAT mode
const modes = [
	{
		// The mode's title
		modeTitle: "Add VAT (net to gross)",

		// An array of objects representing the mode's arguments
		modeArguments: [
			{
				// The HTML element to target for the argument
				target: document.querySelector(".calc-amount > h2"),

				// The new text content for the target
				newTextContent: "Net amount (price exclusive VAT) in euros"
			},
			{
				// The HTML element to target for the argument
				target: document.querySelector(".amount-result > h2"),

				// The new text content for the target
				newTextContent: "Gross result (final price)"
			}
		],

		// A function to execute when this mode is selected
		modeFunction: (baseValue) => {
			// Convert the base value to a number
			const converted = Number(baseValue);

			// Calculate the VAT and the VAT-inclusive result
			let vat = converted * (modeRates[rateValue] / 100);
			let vatResult = converted + (converted * (modeRates[rateValue] / 100));

			// Update the text content of the relevant HTML elements
			document.querySelector(".vat-result > p").textContent = convertToEuro(vat);
			document.querySelector(".amount-result > p").textContent = convertToEuro(vatResult);
		}
	},
	{
		// The mode's title
		modeTitle: "Deduct VAT (gross to net)",

		// An array of objects representing the mode's arguments
		modeArguments: [
			{
				// The HTML element to target for the argument
				target: document.querySelector(".calc-amount > h2"),

				// The new text content for the target
				newTextContent: "Gross amount (price inclusive VAT) in euros"
			},
			{
				// The HTML element to target for the argument
				target: document.querySelector(".amount-result > h2"),

				// The new text content for the target
				newTextContent: "Net result"
			}
		],

		// A function to execute when this mode is selected
		modeFunction: (baseValue) => {
			// Convert the base value to a number
			const converted = Number(baseValue);

			// Calculate the VAT and the VAT-exclusive result
			let vat = converted * (modeRates[rateValue] / 100);
			let vatResult = converted - (converted * (modeRates[rateValue] / 100));

			// Update the text content of the relevant HTML elements
			document.querySelector(".vat-result > p").textContent = convertToEuro(vat);
			document.querySelector(".amount-result > p").textContent = convertToEuro(vatResult);
		}
	}
]

// Declare a variable to hold the rate value
let rateValue;

// An array of mode VAT rates (in percent)
const modeRates = [
	19,
	7
];

// Function to reformat input value with commas and euro symbol
const reformatInput = (input) => {
	// Save current cursor position
	const oldPosition = input.selectionEnd;
	// Replace all non-digit characters and add commas every three digits
	input.value = input.value.replace(/\D/g, '').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + '€';
	// Calculate new cursor position after adding euro symbol
	const newPosition = oldPosition + '€'.length - 1;
	// Move cursor to new position
	input.setSelectionRange(newPosition, newPosition);
};

// Function to calculate VAT based on selected mode and input value
const calculate = () => {
	// Get input value from form
	const inputValue = document.querySelector("input[name='amount']").value;
	// Call the mode function with parsed input value
	modes[modeValue].modeFunction(parseCurrencyString(inputValue));
};

/*
	Switches calculation mode:
	0 = net to gross
	1 = gross to net
 */
const switchMode = () => {
	// Get checked radio button
	const modeInput = document.querySelector("input[name='mode']:checked");
	if (!modeInput) return;

	// Get the label element associated with the input
	const modeLabel = modeInput.parentNode;

	// Update UI based on mode
	for (const current of modes[modeInput.value].modeArguments) {
		current.target.textContent = current.newTextContent;
	}

	// Perform calculation
	calculate();
}

const switchRate = () => {
	// Get checked radio button
	const rateInput = document.querySelector("input[name='rate']:checked");
	if (!rateInput) return;

	// Perform calculation
	calculate();
}

const initializePage = () => {
	// Get the radio button template
	const template = document.querySelector("template");

	// Helper function to create radio buttons
	const createRadioButtons = (container, items, clickHandler, suffix) => {
		// Loop through each item in the `items` array and create a radio button
		items.forEach((item, i) => {
			// Clone the template to create a new radio button
			const clone = template.content.cloneNode(true);
			// Get the input and label elements from the clone
			const input = clone.querySelector('label input[type="radio"]');
			const label = clone.querySelector('label');

			// Configure the input element with various attributes and event listeners
			Object.assign(input, {
				id: `${container}${i}${suffix || ''}`,
				name: container,
				checked: i === 0,
				value: i,
				onclick: clickHandler,
			});

			// Set the `for` attribute of the label to match the input's ID and set the label text
			label.setAttribute('for', input.id);
			label.appendChild(document.createTextNode(`${container === 'mode' ? item.modeTitle : item}${suffix || ''}`));

			// Append the cloned radio button to the appropriate container in the HTML document
			document.querySelector(`.calc-${container}`).appendChild(clone);
		});
	};

	// Call the `createRadioButtons` function to create the mode and rate radio buttons
	createRadioButtons('mode', modes, switchMode);
	createRadioButtons('rate', modeRates, switchRate, "%");
};

// Calls the initializePage function to set up the page
initializePage();

// Calls the switchMode function to switch the calculation mode (e.g. net to gross or gross to net)
//switchMode();

// Calls the switchRate function to switch the VAT rate percentage used in the calculation
//switchRate();