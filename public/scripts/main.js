const tabs = document.querySelector('.tab-list');
const tabButtons = tabs.querySelectorAll('.tab-button');
const tabContent = document.querySelector('#tab-content');

console.log(tabContent)

const displayTab = (index) => {

	const sections = tabContent.children;

	for (let i = 0; i < sections.length; i++) {
		const section = sections[i];
		if (i === index) {
			section.style.transform = `translateX(-${i * 100}%)`;
			section.style.pointerEvents = 'auto';
		} else {
			section.style.transform = `translateX(${index > i ? '-' : ''}${index * 100}%)`;
			section.style.pointerEvents = 'none';
		}
	}
}
tabButtons.forEach((button, index) => {
	button.addEventListener('click', () => {
		displayTab(index);
		tabButtons.forEach((button, i) => {
			button.classList.toggle('active', i === index);
		});
	});
});