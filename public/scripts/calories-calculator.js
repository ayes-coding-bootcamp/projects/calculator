const caloriesCalculator = document.querySelector("#calories-calculator");

activities = new Map();
activities.set("sleep", 0.95);
activities.set("sitting or laying", 1.2);
activities.set("sedentary job/lifestyle with little or no physical activity in leisure time", 1.5);
activities.set("sedentary job with occasional walking or standing", 1.7);
activities.set("mostly standing or walking job", 1.9);
activities.set("physically demanding job/work", 2.2);

updateActivityIndex = (newIndex) => {
	const target = caloriesCalculator.querySelector(".dropdown-menu > button");

	target.textContent = caloriesCalculator.querySelectorAll(".dropdown-content > button")[newIndex].textContent;
}

caloriesCalculator.querySelectorAll(".dropdown-content > button").forEach((button, index) => {
	button.addEventListener('click', () => {
		updateActivityIndex(index);
		caloriesCalculator.querySelector(".dropdown-content").style.display = "none";
	});
});